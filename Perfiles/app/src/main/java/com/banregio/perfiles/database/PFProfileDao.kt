package com.banregio.perfiles.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.banregio.perfiles.data.PFProfileData

@Dao
interface PFProfileDao {

    @Query("SELECT * FROM PFProfileData")
    fun getAllProfiles() : List<PFProfileData>

    @Query("SELECT * FROM PFProfileData WHERE id = :id")
    fun findById(id: Int): PFProfileData

    @Query("SELECT COUNT(id) FROM PFProfileData")
    fun getCount(): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(profile: PFProfileData)

    @Update
    fun update(profile: PFProfileData)

    @Delete
    fun delete(profile: PFProfileData)

}