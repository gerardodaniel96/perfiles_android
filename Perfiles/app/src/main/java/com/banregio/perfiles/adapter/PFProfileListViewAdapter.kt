package com.banregio.perfiles.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import android.util.Base64
import com.banregio.perfiles.R
import com.banregio.perfiles.data.PFProfileData

class PFProfileListViewAdapter(private val profileContext: Context, private val profileList: List<PFProfileData>)
    : ArrayAdapter<PFProfileData>(profileContext, 0, profileList) {
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(profileContext).inflate(R.layout.profile_view, parent, false)

        val profile = profileList[position]

        val profileImage: ImageView = layout.findViewById(R.id.ivProfileView)
        val profileName: TextView = layout.findViewById(R.id.tvProfileViewName)
        val profilePhone: TextView = layout.findViewById(R.id.tvProfileViewPhone)
        val profileEmail: TextView = layout.findViewById(R.id.tvProfileViewEmail)

        profileName.text = profile.name
        profilePhone.text = profile.phone
        profileEmail.text = profile.email

        if (profile.image.isNotEmpty()) {
            val imageBytes = Base64.decode(profile.image, Base64.DEFAULT)
            val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            profileImage.setImageBitmap(decodedImage)
        }

        return layout
    }
}