package com.banregio.perfiles.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class PFProfileData(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var name: String,
    var phone: String,
    var email: String,
    var image: String
) : Serializable
