package com.banregio.perfiles.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.banregio.perfiles.data.PFProfileData

@Database(
    entities = [PFProfileData::class],
    version = 1
)
abstract class PFProfileDB : RoomDatabase() {
    abstract fun profileDao(): PFProfileDao
}