package com.banregio.perfiles.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ListView
import androidx.room.Room
import com.banregio.perfiles.data.PFProfileData
import com.banregio.perfiles.adapter.PFProfileListViewAdapter
import com.banregio.perfiles.R
import com.banregio.perfiles.database.PFProfileDB

class PFMainActivity : AppCompatActivity() {

    private lateinit var buttonView: Button
    private lateinit var buttonCreate: Button
    private lateinit var lvProfiles: ListView

    private var selectedPosition: Int = 0
    private var listOfProfiles: List<PFProfileData> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_Perfiles)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
    }

    override fun onResume() {
        super.onResume()
        buttonView.isEnabled = false
        selectedPosition = 0
        getProfilesFromMemory()
        setListViewAdapter()
    }

    private fun setupView() {
        setupComponents()
        getProfilesFromMemory()
        setListViewAdapter()
    }

    private fun setupComponents() {
        lvProfiles = findViewById(R.id.lvProfiles)
        buttonView = findViewById(R.id.buttonView)
        buttonCreate = findViewById(R.id.buttonCreate)
        buttonView.isEnabled = false
        buttonView.setOnClickListener {
            showProfile()
        }
        buttonCreate.setOnClickListener {
            createProfile()
        }
        lvProfiles.setOnItemClickListener { _, _, position, _ ->
            selectedPosition = position
            buttonView.isEnabled = true
        }
    }

    private fun getProfilesFromMemory() {
        val db = Room.databaseBuilder(applicationContext, PFProfileDB::class.java, "profiles").allowMainThreadQueries().build()
        val dao = db.profileDao()
        listOfProfiles = dao.getAllProfiles()
    }

    private fun setListViewAdapter() {
        val adapter = PFProfileListViewAdapter(this, listOfProfiles)
        lvProfiles.adapter = adapter
    }

    private fun showProfile() {
        // Get profile data
        val profile: PFProfileData = listOfProfiles[selectedPosition]

        // Create intent with user data
        val intent = Intent(this, PFProfileActivity::class.java)
        intent.putExtra("profileData", profile)

        // Start new activity for MAUserActivity
        startActivity(intent)
    }

    private fun createProfile() {
        val intent = Intent(this, PFProfileActivity::class.java)
        startActivity(intent)
    }

}