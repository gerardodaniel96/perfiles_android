package com.banregio.perfiles.activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.room.Room
import com.banregio.perfiles.data.PFProfileData
import com.banregio.perfiles.R
import com.banregio.perfiles.database.PFProfileDB
import java.io.ByteArrayOutputStream

class PFProfileActivity : AppCompatActivity() {

    private lateinit var etName: TextView
    private lateinit var etPhone: TextView
    private lateinit var etEmail: TextView
    private lateinit var buttonSave: Button
    private lateinit var buttonDelete: Button
    private lateinit var ivProfilePic: ImageView

    private var profile: PFProfileData? = null
    private var didSelectImage: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setupView()
        getDataFromIntent()
    }

    private fun setupView() {
        etName = findViewById(R.id.etName)
        etPhone = findViewById(R.id.etPhone)
        etEmail = findViewById(R.id.etEmail)
        buttonSave = findViewById(R.id.buttonSave)
        ivProfilePic = findViewById(R.id.ivProfile)
        buttonDelete = findViewById(R.id.buttonDelete)
        buttonSave.setOnClickListener { saveProfile() }
        buttonDelete.setOnClickListener { deleteProfile() }
        ivProfilePic.setOnClickListener { pickImageFromGallery() }
    }

    private fun getDataFromIntent() {
        // Get data from intent
        profile = intent.getSerializableExtra("profileData") as? PFProfileData
        if (profile == null) return

        // Set the view components with the user data (from intent)
        profile?.let {
            etName.text = it.name
            etPhone.text = it.phone
            etEmail.text = it.email
            if (it.image.isNotEmpty()) {
                val imageBytes = Base64.decode(it.image, Base64.DEFAULT)
                val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                ivProfilePic.setImageBitmap(decodedImage)
            }
        }

        // Make button visible
        buttonDelete.visibility = View.VISIBLE
    }

    private fun validateData() : Boolean {
        if (profile != null) return true

        profile?.let {
            if (etName.text != it.name || etPhone.text != it.phone || etEmail.text != it.email) return true
        }

        return false
    }

    private fun updateCurrentProfile() {
        profile?.name = etName.text.toString()
        profile?.phone = etPhone.text.toString()
        profile?.email = etEmail.text.toString()
        profile?.image = getStringFromImage()
    }

    private fun saveProfile() {
        val db = Room.databaseBuilder(applicationContext, PFProfileDB::class.java, "profiles").allowMainThreadQueries().build()
        val dao = db.profileDao()
        if (validateData()) {
            updateCurrentProfile()
            profile?.let {
                dao.update(it)
            }
        }
        else {
            profile = PFProfileData(
                dao.getCount() + 1,
                etName.text.toString(),
                etPhone.text.toString(),
                etEmail.text.toString(),
                if (didSelectImage) getStringFromImage() else ""
            )
            profile?.let {
                dao.insert(it)
            }
        }
        finish()
    }

    private fun deleteProfile() {
        val db = Room.databaseBuilder(applicationContext, PFProfileDB::class.java, "profiles").allowMainThreadQueries().build()
        val dao = db.profileDao()
        profile?.let {
            dao.delete(it)
        }
        finish()
    }

    private fun pickImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(intent, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == RESULT_OK) {
            ivProfilePic.setImageURI(data?.data)
            didSelectImage = true
        }
    }

    private fun getStringFromImage(): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        val bitmap = ivProfilePic.drawable.toBitmap()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val imageBytes: ByteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(imageBytes, Base64.DEFAULT)
    }

}